package com.crocodev.tegar.dokumentasi.model;

/**
 * Created by j on 17/04/2017.
 */

public class CurahHujan {
    private String kecamatan, kelurahan, curah;

    public CurahHujan() {
    }

    public CurahHujan(String kecamatan, String kelurahan, String curah) {
        this.kecamatan = kecamatan;
        this.kelurahan = kelurahan;
        this.curah = curah;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getCurah() {
        return curah;
    }

    public void setCurah(String curah) {
        this.curah = curah;
    }
}
