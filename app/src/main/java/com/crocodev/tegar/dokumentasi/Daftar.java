package com.crocodev.tegar.dokumentasi;

import java.util.ArrayList;

/**
 * Created by j on 02/04/2017.
 * kelas ini berisi array of string
 * yang digunakan untuk spinner
 */

public class Daftar {
    String [] tipe = {
            "Tipe","Banjir", "Genangan"
    };
    String [] rayon = {
            "Pilih Rayon / Wilayah",
            "Surabaya Pusat",
            "Surabaya Utara",
            "Surabaya Timur",
            "Surabaya Selatan",
            "Surabaya Barat"
    };
    String [] level = {"Level","Sedang" , "Rendah", "Tinggi"
    };
    String [] jenis = {"Jenis","Wilayah", "Jalan"
    };

    public String[] getJenisdrainase() {
        return jenisdrainase;
    }

    public void setJenisdrainase(String[] jenisdrainase) {
        this.jenisdrainase = jenisdrainase;
    }

    String [] jenisdrainase = {"a","b","c","d"};
    ArrayList<String[][]> kelurahan6 = new ArrayList<>();

    public ArrayList<String[][]> getKelurahan6() {
        String [][] def = {{"Pilih Kelurahan"}};
        kelurahan6.add(0, def);
        kelurahan6.add(0, kelurahan1);
        kelurahan6.add(1, kelurahan2);
        kelurahan6.add(2, kelurahan3);
        kelurahan6.add(3, kelurahan4);
        kelurahan6.add(4, kelurahan5);
        return kelurahan6;
    }

    String [][] kecamatan1 = {
            {"Pilih Kecamatan"},
            {"Asemrowo", "Bubutan", "Genteng", "Gubeng", "Sawahan"},
            {"Bulak", "Kenjeran", "Krembangan", "Pabean Cantikan", "Semampir", "Simokerto"},
            {"Gununganyar", "Mulyorejo", "Rungkut", "Sukolilo", "Tambaksari", "Tenggilis Mejoyo"},
            {"Dukuh Pakis", "Gayungan", "Jambangan", "Tegal Sari", "Wonocolo", "Wonokromo"},
            {"Benowo", "Karangpilang", "Lakar Santri", "Pakal", "Sambikerep", "Sukomanunggal", "Tandes", "Wiyung"}
    };

    String [][] kecamatan = {
            {"Pilih Kecamatan"},
            {"Asemrowo", "Bubutan", "Genteng", "Gubeng", "Sawahan"},
            {"Bulak", "Kenjeran", "Krembangan", "Pabean Cantikan", "Semampir", "Simokerto"},
            {"Gununganyar", "Mulyorejo", "Rungkut", "Sukolilo", "Tambaksari", "Tenggilis Mejoyo"},
            {"Dukuh Pakis", "Gayungan", "Jambangan", "Tegal Sari", "Wonocolo", "Wonokromo"},
            {"Benowo", "Karangpilang", "Lakar Santri", "Pakal", "Sambikerep", "Sukomanunggal", "Tandes", "Wiyung"}
    };

    public String[] getRayon() {
        return rayon;
    }

    public String[][] getKecamatan1() {
        return kecamatan1;
    }

    public String[][] getKelurahan1() {
        return kelurahan1;
    }

    public String[][] getKelurahan2() {
        return kelurahan2;
    }

    public String[][] getKelurahan3() {
        return kelurahan3;
    }

    public String[][] getKelurahan4() {
        return kelurahan4;
    }

    public String[][] getKelurahan5() {
        return kelurahan5;
    }

    String [][] kelurahan1 = {
            {
                    "Asemrowo",
                    "Genting",
                    "Greges",
                    "Kalianak",
                    "Tambak Langon"
            },{"Jepara",
            "Gundih",
            "Tembok Dukuh",
            "Alon Alon Contong",
            "Bubutan"},
            {"Embong Kaliasin",
                    "Ketabang",
                    "Kapasari",
                    "Peneleh",
                    "Genteng"},
            {"Gubeng",
                    "Kertajaya",
                    "Pucang Sewu",
                    "Baratajaya",
                    "Mojo",
                    "Airlangga"},
            {"Sawahan",
                    "Petemon",
                    "Kupang Krajan",
                    "Banyu Urip",
                    "Putat Jaya",
                    "Pakis"}
    }, kelurahan2 = {
            {"Komplek Kenjeran",
                    "Sukolilo",
                    "Kenjeran",
                    "Bulak",
                    "Kedung Cowek"},
            {"Tambak Wedi",
                    "Bulak Banteng",
                    "Sidotopo/Sidotopo Wetan",
                    "Tanah Kali Kedinding"},
            {"Krembangan Selatan",
                    "Kemayoran",
                    "Perak Barat",
                    "Moro Krembagan",
                    "Dupak"},
            {"Bongkaran",
                    "Nyamplungan",
                    "Krembangan Utara",
                    "Perak Timur",
                    "Perak Utara"},
            {"Ampel",
                    "Sidotopo",
                    "Pegirian",
                    "Wonokusumo",
                    "Ujung"},
            {"Kapasan",
                    "Tambakrejo",
                    "Simokerto",
                    "Simolawang",
                    "Sidodadi"}
    }, kelurahan3 = {
            {"Rungkut Menanggal",
                    "Rungkut Tengah",
                    "Gunung Anyar",
                    "Gunung Anyar Tambak"},
            {"Kalisari",
                    "Kejawen Putih Tambak",
                    "Dukuh Sutorejo",
                    "Kalijudan",
                    "Mulyorejo",
                    "Manyar Sabrangan"},
            {"Kali Rungkut",
                    "Rungkut Kidul",
                    "Medokan Ayu",
                    "Wonorejo",
                    "Penjaringan Sari",
                    "Kedung Baruk"},
            {"Keputih",
                    "Gebang Putih",
                    "Klampis Ngasem",
                    "Menur Pumpungan",
                    "Nginden Jangkungan",
                    "Medokan Semampir",
                    "Semolowaru"},
            {"Pacar Keling",
                    "Pacar Kembang",
                    "Ploso",
                    "Gading",
                    "Rangkah",
                    "Tambaksari"},
            {"Kutisari",
                    "Kendangsari",
                    "Tenggilis Mejoyo",
                    "Panjang Jiwo",
                    "Prapen"}
    }, kelurahan4 = {

            {"Gunungsari",
                    "Dukuh Kupang",
                    "Dukuh Pakis",
                    "Pradah Kali Kendal"},
            {"Ketintang",
                    "Dukuh Menanggal",
                    "Menanggal",
                    "Gayungan"},
            {"Jambangan",
                    "Karah",
                    "Kebonsari",
                    "Pagesangan"},
            {"Kedungdoro",
                    "Tegalsari",
                    "Wonorejo",
                    "Dr. Sutomo",
                    "Keputran"},
            {"Siwalankerto",
                    "Jemur Wonosari",
                    "Margorejo",
                    "Bendul Merisi",
                    "Sidosermo"},
            {"Darmo",
                    "Sawunggaling",
                    "Wonokromo",
                    "Jagir",
                    "Ngagelrejo",
                    "Ngagel"}
    }, kelurahan5 = {

            {
                    "Tambakoso Wilangon",
                    "Romokalisari",
                    "Klakah Rejo",
                    "Sememi",
                    "Kandangan"
            },
            {"Karang Pilang",
                    "Waru Gunung",
                    "Kebraon",
                    "Kedurus"},
            {"Lakar Santri",
                    "Jeruk",
                    "Lidah Kulon",
                    "Lidah Wetan",
                    "Bangkingan",
                    "Sumur Welut"},
            {"Sombe Rejo",
                    "Tambak Dono",
                    "Benowo",
                    "Pakal",
                    "Babat Jerawat"},
            {"Lontar",
                    "Sambikerep",
                    "Bringin",
                    "Made"},
            {"Tanjungsari",
                    "Suko Manunggal",
                    "Putat Gede",
                    "Sonokwijenan",
                    "Simomulyo"},
            {"Buntaran",
                    "Banjar Sugian",
                    "Manukan Kulon",
                    "Manukan Wetan",
                    "Balong Sari",
                    "Bibis",
                    "Gedang Asin",
                    "Karang Poh",
                    "Tandes Kidul",
                    "Tandes Lor",
                    "Gadel",
                    "Tubanan"},
            {"Balas Klumprik",
                    "Babatan",
                    "Wiyung",
                    "Jajartunggal"}
    };

    String[] getJenis() {
        return jenis;
    }

    String[] getTipe() {
        return tipe;
    }

    String[] getLevel(){return level;}
}
