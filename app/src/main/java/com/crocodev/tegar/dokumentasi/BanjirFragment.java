package com.crocodev.tegar.dokumentasi;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.crocodev.tegar.dokumentasi.application.DokumentasiApp;
import com.crocodev.tegar.dokumentasi.model.Banjir;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class BanjirFragment extends Fragment implements AdapterView.OnItemSelectedListener,OnMapReadyCallback{
    Daftar daftar = new Daftar();
    Banjir banjir = new Banjir();
    String uri, alamat,  locality;
    ProgressBar progressBar;
    String [] def = {"Pilih Kelurahan"};
    Spinner tipe, jenis, rayon, camat, lurah,level;
    ImageView foto;
    EditText kronologi, lat,lng, korban, luas, lama;
    static Button tanggal,kirim,cari,lokasi;

    GoogleMap mGoogleMap;
    MapView mMapView;
    View  mView ;

    public BanjirFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         mView = inflater.inflate(R.layout.fragment_banjir, container, false);

        rayon      = (Spinner) mView.findViewById         (R.id.rayon);
        camat      = (Spinner) mView.findViewById     (R.id.kecamatan);
        lurah      = (Spinner) mView.findViewById     (R.id.kelurahan);
        tanggal    = (Button) mView.findViewById        (R.id.tanggal);
        jenis      = (Spinner) mView.findViewById  (R.id.jenis_banjir);
        tipe       = (Spinner) mView.findViewById   (R.id.tipe_banjir);
        level      = (Spinner) mView.findViewById  (R.id.level_banjir);
        foto       = (ImageView) mView.findViewById (R.id.foto_banjir);
       // koordinat  = (EditText) mView.findViewById    (R.id.koordinat);

        kronologi  = (EditText) mView.findViewById    (R.id.kronologi);
        korban     = (EditText) mView.findViewById  (R.id.korban_jiwa);
        luas       = (EditText) mView.findViewById  (R.id.luas_banjir);
        lama       = (EditText) mView.findViewById  (R.id.lama_banjir);

        progressBar = (ProgressBar) mView.findViewById(R.id.progressBar);
        kirim = (Button) mView.findViewById(R.id.fab);

        lokasi = (Button) mView.findViewById(R.id.btn_lokasi);
        initialize();

        return mView;
    }
    private void showDialog(){
        final Dialog banjirdialog = new Dialog(getContext());
        banjirdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        banjirdialog.setContentView(R.layout.mapdialog);
        banjirdialog.show();
        Button batal = (Button) banjirdialog.findViewById(R.id.btn_cncl),
        simpan = (Button) banjirdialog.findViewById(R.id.btn_save);

        mMapView=(MapView) mView.findViewById(R.id.mapFragment);
        cari = (Button) banjirdialog.findViewById(R.id.btn_cari);
        lat        = (EditText) banjirdialog.findViewById(R.id.lat);
        lng        = (EditText) banjirdialog.findViewById(R.id.lng);

        MapsInitializer.initialize(getActivity());

        mMapView = (MapView) banjirdialog.findViewById(R.id.mapFragment);
        mMapView.onCreate(banjirdialog.onSaveInstanceState());
        mMapView.onResume();// needed to get the map to display immediately
        banjirdialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mMapView.getMapAsync(this);

        // pencarian geolocate
        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    cari(banjirdialog);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                banjirdialog.dismiss();
            }
        });

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                banjir.setLat(lat.getText().toString());
                banjir.setLng(lng.getText().toString());
                banjirdialog.dismiss();
            }
        });
    }
    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        if(mMapView != null){
//            mMapView.onCreate(null);
//            mMapView.onResume();
//            mMapView.getMapAsync(this);
//        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(getContext());
        mGoogleMap=googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(-7.2756368,112.6416433)).title("Surabaya").snippet("Kota Pahlawan"));
        CameraPosition Surabaya = CameraPosition.builder().target(new LatLng(-7.2756368,112.6416433)).zoom(16).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(Surabaya));

        if ((ActivityCompat.checkSelfPermission(mView.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(mView.getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            mGoogleMap.setMyLocationEnabled(true);
        }

        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                lat.setText(String.valueOf(marker.getPosition().latitude));
                lng.setText(String.valueOf(marker.getPosition().longitude));
                marker.showInfoWindow();
                return true;
            }
        });
    }

    private void goToLocationZoom(double lat, double lng, float zoom) {
        LatLng ll = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        mGoogleMap.moveCamera(update);
    }

    Marker marker;
    void cari(Dialog mView) throws IOException {

        EditText et = (EditText) mView.findViewById(R.id.editText);
        String location = et.getText().toString();

        Geocoder gc = new Geocoder(mView.getContext());
        List<Address> list = gc.getFromLocationName(location, 1);
        Address address = list.get(0);
        locality = address.getLocality();
        alamat = address.getAddressLine(0);

        Toast.makeText(mView.getContext(), locality+", "+alamat+", max address line = "+address.getMaxAddressLineIndex(), Toast.LENGTH_LONG).show();

        double lat = address.getLatitude();
        double lng = address.getLongitude();
        goToLocationZoom(lat, lng, 15);

        setMarker(locality, lat, lng);
    }

    private void setMarker(String locality, double lat, double lng) {
        if(marker!=null){
            marker.remove();
        }

        MarkerOptions options = new MarkerOptions()
                .title(locality)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .position(new LatLng(lat,lng))
                .snippet("aku disini");

        marker = mGoogleMap.addMarker(options);
    }

    void initialize(){
        ArrayAdapter<String> adapter;
        //koordinat

        // rayon
        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, daftar.getRayon());
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        rayon.setAdapter(adapter);

        // kecamatan
        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, def);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        camat.setAdapter(adapter);

        // kelurahan
        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, def);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        lurah.setAdapter(adapter);

        // jenis
        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, daftar.getJenis());
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        jenis.setAdapter(adapter);

        // tipe
        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, daftar.getTipe());
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        tipe.setAdapter(adapter);
        //level
        adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item,daftar.getLevel());
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        level.setAdapter(adapter);

        // pilih gambar
        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        // kirim
        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kirim();
            }
        });


        // kalender
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        String formattedDate = sdf.format(c.getTime());
        tanggal.setText(formattedDate);
        tanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
        rayon.setOnItemSelectedListener(this);
        camat.setOnItemSelectedListener(this);
        lokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        SetSpinnerList ssl = new SetSpinnerList(rayon, camat, lurah, this);
        ssl.onItemSelected(parent, view, position, id);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    void kirim(){
        banjir.setTipe(tipe.getSelectedItem().toString());
        banjir.setTanggal(tanggal.getText().toString());
        banjir.setRayon(rayon.getSelectedItem().toString());
        banjir.setKecamatan(camat.getSelectedItem().toString());
        banjir.setKelurahan(lurah.getSelectedItem().toString());
        banjir.setLat(lat.getText().toString());
        banjir.setLng(lng.getText().toString());
        banjir.setKronologi(kronologi.getText().toString());
        banjir.setKorban(korban.getText().toString());
        if (!hasImage(foto)) banjir.setFoto("");
        else banjir.setFoto(uri);
        banjir.setLuas(luas.getText().toString());
        banjir.setLama(lama.getText().toString());
        banjir.setJenis(jenis.getSelectedItem().toString());
        new KirimKeAPI().execute(banjir);
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            foto.setImageBitmap(imageBitmap);
            uri = data.getData().toString();
        }
    }

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable)drawable).getBitmap() != null;
        }

        return hasImage;
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Calendar c = Calendar.getInstance();
            c.set(year, month, dayOfMonth);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            String formattedDate = sdf.format(c.getTime());
            tanggal.setText(formattedDate);
        }
    }

    private class KirimKeAPI extends AsyncTask<Banjir, Void, Banjir> {

        @Override
        protected Banjir doInBackground(Banjir... params) {
            MediaType mt = MediaType.parse("application/x-www-form-urlencoded");
            OkHttpClient client = new OkHttpClient();

            RequestBody formBody = new FormBody.Builder()
                    .add("tipe_banjir", params[0].getTipe())
//                    .add("tanggal", params[0].getTanggal())
                    .add("rayon", params[0].getRayon())
                    .add("kecamatan", params[0].getKecamatan())
                    .add("kelurahan", String.valueOf(params[0].getKelurahan()))
                    .add("lat", String.valueOf(params[0].getLat()))
                    .add("lng", String.valueOf(params[0].getLng()))
                    .add("kronologi", params[0].getKronologi())
                    .add("korban_jiwa", String.valueOf(params[0].getKorban()))
                    .add("foto", String.valueOf(params[0].getFoto()))
                    .add("luas", params[0].getLuas())
                    .add("lama", String.valueOf(params[0].getLama()))
                    .add("jenis", String.valueOf(params[0].getJenis()))
                    .build();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(DokumentasiApp.getContext());

            String link = "http://"+sharedPreferences.getString("ip_address", "192.168.1.26")+DokumentasiApp.getUrlApiInsertBanjir();
            Request request = new Request.Builder()
                    .url(link)
                    .post(formBody)
                    .build();

            try {
                Response response = client.newCall(request).execute();
            } catch (IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(DokumentasiApp.getContext(), "Gagal", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Toast.makeText(DokumentasiApp.getContext(), "Mengirim", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Banjir banjir) {
//            Toast.makeText(DokumentasiApp.getContext(), "Cek DB", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.INVISIBLE);
            tipe.setSelection(0);
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            String formattedDate = sdf.format(c.getTime());
            tanggal.setText(formattedDate);
            rayon.setSelection(0);
            lat.setText("");
            lng.setText("");
            kronologi.setText("");
            korban.setText("");
            foto.setImageResource(R.drawable.ic_camera);
            luas.setText("");
            lama.setText("");
            jenis.setSelection(0);
        }

    }
}
