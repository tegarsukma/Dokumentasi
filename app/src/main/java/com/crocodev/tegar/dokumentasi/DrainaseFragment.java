package com.crocodev.tegar.dokumentasi;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.crocodev.tegar.dokumentasi.model.Drainase;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class DrainaseFragment extends Fragment implements AdapterView.OnItemSelectedListener,OnMapReadyCallback{
    Daftar daftar = new Daftar();
    Drainase drainase = new Drainase();
    GoogleMap mGoogleMap;
    MapView mMapView;
    View  mView ;
    Spinner rayon, camat, lurah,jenis;
    static Button cari,tgl,lokasi;
    EditText lat,lng;
    String [] def = {"Pilih Kelurahan"};
    public DrainaseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_drainase, container, false);
        rayon      = (Spinner) mView.findViewById         (R.id.rayon);
        camat      = (Spinner) mView.findViewById     (R.id.kecamatan);
        lurah      = (Spinner) mView.findViewById     (R.id.kelurahan);
        jenis      = (Spinner) mView.findViewById  (R.id.jenis_drainase);
        cari = (Button) mView.findViewById(R.id.btn_cari);
        tgl = (Button) mView.findViewById(R.id.tglDrainase);
        lokasi = (Button) mView.findViewById(R.id.btn_lokasi_drainase);
        initialize();
        return mView;
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       // mMapView=(MapView) mView.findViewById(R.id.mapDrainaseFragment);
//        if(mMapView != null){
//            mMapView.onCreate(null);
//            mMapView.onResume();
//            mMapView.getMapAsync(this);
//        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());
        mGoogleMap=googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(-7.2756368,112.6416433)).title("Surabaya").snippet("Kota Pahlawan"));
        CameraPosition Surabaya = CameraPosition.builder().target(new LatLng(-7.2756368,112.6416433)).zoom(16).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(Surabaya));

        if ((ActivityCompat.checkSelfPermission(mView.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(mView.getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    private void goToLocationZoom(double lat, double lng, float zoom) {
        LatLng ll = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        mGoogleMap.moveCamera(update);
    }

    Marker marker;
    void cari(Dialog mView) throws IOException {

        EditText et = (EditText) mView.findViewById(R.id.editText);
        String location = et.getText().toString();

        Geocoder gc = new Geocoder(mView.getContext());
        List<Address> list = gc.getFromLocationName(location, 1);
        Address address = list.get(0);
        String locality = address.getLocality();

        Toast.makeText(mView.getContext(), locality, Toast.LENGTH_LONG).show();

        double lat = address.getLatitude();
        double lng = address.getLongitude();
        goToLocationZoom(lat, lng, 15);

        setMarker(locality, lat, lng);
    }

    private void setMarker(String locality, double lat, double lng) {
        if(marker!=null){
            marker.remove();
        }

        MarkerOptions options = new MarkerOptions()
                .title(locality)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .position(new LatLng(lat,lng))
                .snippet("aku disini");

        marker = mGoogleMap.addMarker(options);
    }
    void initialize(){
        ArrayAdapter<String> adapter;

        // rayon
        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, daftar.getRayon());
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        rayon.setAdapter(adapter);

        // kecamatan
        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, def);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        camat.setAdapter(adapter);

        // kelurahan
        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, def);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        lurah.setAdapter(adapter);

//        cari.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    cari();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
        // jenis
        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, daftar.getJenisdrainase());
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        jenis.setAdapter(adapter);

        // kalender
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        String formattedDate = sdf.format(c.getTime());
        tgl.setText(formattedDate);
        tgl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        rayon.setOnItemSelectedListener(this);
        camat.setOnItemSelectedListener(this);

        lokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }

    private void showDialog(){
        final Dialog drainasedialog = new Dialog(getContext());
        drainasedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        drainasedialog.setContentView(R.layout.drainasedialog);
        drainasedialog.show();
        Button batal = (Button) drainasedialog.findViewById(R.id.btn_cncl),
                simpan = (Button)drainasedialog.findViewById(R.id.btn_save);

        mMapView=(MapView) mView.findViewById(R.id.mapFragment);
        cari = (Button) drainasedialog.findViewById(R.id.btn_cari_drainase);
        lat        = (EditText) drainasedialog.findViewById(R.id.lat);
        lng        = (EditText) drainasedialog.findViewById(R.id.lng);

        MapsInitializer.initialize(getActivity());

        mMapView = (MapView) drainasedialog.findViewById(R.id.mapFragment);
        mMapView.onCreate(drainasedialog.onSaveInstanceState());
        mMapView.onResume();// needed to get the map to display immediately
        drainasedialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mMapView.getMapAsync(this);

        // pencarian geolocate
        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    cari(drainasedialog);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drainasedialog.dismiss();
            }
        });

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drainase.setLat(lat.getText().toString());
                drainase.setLng(lng.getText().toString());
                drainasedialog.dismiss();
            }
        });
    }

    public void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragmentDrainase();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public static class DatePickerFragmentDrainase extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Calendar c = Calendar.getInstance();
            c.set(year, month, dayOfMonth);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            String formattedDate = sdf.format(c.getTime());
            tgl.setText(formattedDate);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String select = parent.getSelectedItem().toString();
        ArrayAdapter<String> adapter;
        Log.i("spinner", "select = "+select);
        Log.i("spinner", "position from method = "+position);
        Log.i("spinner", "id from method = "+id);
        Log.i("spinner", "camat id = "+camat.getId()+", lurah id = "+lurah.getId());
        Log.i("spinner", "kecamatan id = "+R.id.kecamatan+", kelurahan id = "+R.id.kelurahan);
//        Log.i("spinner", "view = " + view.);
        Log.i("spinner", "parent = "+parent.getId());
        switch (parent.getId()){
            case R.id.rayon:
                Toast.makeText(getActivity(), "Click on Rayon Spinner", Toast.LENGTH_SHORT).show();
                adapter = new ArrayAdapter<>(getActivity(),
                        android.R.layout.simple_spinner_item, daftar.getKecamatan1()[position]);
                adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                camat.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;
            case R.id.kecamatan:
                Toast.makeText(getActivity(), "Click on Kecamatan Spinner", Toast.LENGTH_SHORT).show();
                switch (select){
                    case "Asemrowo":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan1()[0]);
                        break;
                    case "Bubutan":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan1()[1]);
                        break;
                    case "Genteng":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan1()[2]);
                        break;
                    case "Gubeng":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan1()[3]);
                        break;
                    case "Sawahan":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan1()[4]);
                        break;
                    case "Bulak":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan2()[0]);
                        break;
                    case "Kenjeran":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan2()[1]);
                        break;
                    case "Krembangan":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan2()[2]);
                        break;
                    case "Pabean Cantikan":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan2()[3]);
                        break;
                    case "Semampir":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan2()[4]);
                        break;
                    case "Simokerto":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan2()[5]);
                        break;
                    case "Gununganyar":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan3()[0]);
                        break;
                    case "Mulyorejo":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan3()[1]);
                        break;
                    case "Rungkut":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan3()[2]);
                        break;
                    case "Sukolilo":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan3()[3]);
                        break;
                    case "Tambaksari":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan3()[4]);
                        break;
                    case "Tenggilis Mejoyo":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan3()[5]);
                        break;
                    case "Dukuh Pakis":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan4()[0]);
                        break;
                    case "Gayungan":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan4()[1]);
                        break;
                    case "Jambangan":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan4()[2]);
                        break;
                    case "Tegal Sari":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan4()[3]);
                        break;
                    case "Wonocolo":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan4()[4]);
                        break;
                    case "Wonokromo":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan4()[5]);
                        break;
                    case "Benowo":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[0]);
                        break;
                    case "Karang Pilang":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[1]);
                        break;
                    case "Lakar Santri":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[2]);
                        break;
                    case "Pakal":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[3]);
                        break;
                    case "Sambikerep":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[4]);
                        break;
                    case "Sukomanunggal":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[5]);
                        break;
                    case "Tandes":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[6]);
                        break;
                    case "Wiyung":
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[7]);
                        break;
                    default:
                        adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_spinner_item, def);
                }
//                adapter = new ArrayAdapter<>(getActivity(),
//                                android.R.layout.simple_spinner_item, daftar.getKelurahan6().get(position)[0]);
                adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                lurah.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
