package com.crocodev.tegar.dokumentasi.application;

import android.app.Application;
import android.content.Context;

/**
 * Created by j on 04/04/2017.
 */

public class DokumentasiApp extends Application {
    private static Application application;

    public static Context getContext() {
        return application.getApplicationContext();
    }

    public void onCreate() {
        super.onCreate();
        application = this;
}
    public static String getUrlApiInsertBanjir(){
        return "/floodizy/backend/web/api/banjir/create";
    }
    public static String getUrlApiGetAllBanjir(){
        return "/floodizy/backend/web/api/banjir/get-all";
    }
    public static String getUrlApiGetOneBanjir(String id){
        return "/floodizy/backend/web/api/banjir/get-one?id="+id;
    }
    public static String getUrlApiCreateKePen(){
        return "/floodizy/backend/web/api/kepadatan-penduduk/create";
    }
    public static String getUrlApiGetAllKePen(){
        return "/floodizy/backend/web/api/kepadatan-penduduk/get-all";
    }
    public static String getUrlApiGetOneKePen(String id){
        return "/floodizy/backend/web/api/kepadatan-penduduk/get-one?id="+id;
    }
    public static String getUrlApiCreateCuHu(){
        return "/floodizy/backend/web/api/curah-hujan/create";
    }
    public static String getUrlApiGetAllCuHu(){
        return "/floodizy/backend/web/api/curah-hujan/get-all";
    }
    public static String getUrlApiGetOneCuhu(String id){
        return "/floodizy/backend/web/api/curah-hujan/get-one?id="+id;
    }
}
