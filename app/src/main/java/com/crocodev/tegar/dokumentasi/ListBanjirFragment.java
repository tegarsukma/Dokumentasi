package com.crocodev.tegar.dokumentasi;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.crocodev.tegar.dokumentasi.application.DokumentasiApp;
import com.crocodev.tegar.dokumentasi.model.Banjir;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListBanjirFragment extends Fragment implements OnMapReadyCallback{
    GoogleMap mGoogleMap;
    MapView mMapView;
    View  mView;
    SwipeRefreshLayout srl;
    ArrayList<Banjir> data;
    ProgressBar pb;
    RecyclerView recyclerView;
    CustomAdapter adapter;

    public ListBanjirFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_list_banjir, container, false);
        pb = (ProgressBar) mView.findViewById(R.id.pb_list_banjir);
        pb.setVisibility(View.INVISIBLE);
        getData();
        srl = (SwipeRefreshLayout) mView.findViewById(R.id.srl_list_banjir);
        recyclerView = (RecyclerView) mView.findViewById(R.id.recyclerBanjir);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });
        return mView;
    }

    private void refreshItems() {
        getData();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        mMapView=(MapView) mView.findViewById(R.id.listmapFragment);
//        if(mMapView != null){
//            mMapView.onCreate(null);
//            mMapView.onResume();
//            mMapView.getMapAsync(this);
//        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
//        MapsInitializer.initialize(getContext());
//        mGoogleMap=googleMap;
//        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(-7.2756368,112.6416433)).title("Surabaya").snippet("Kota Pahlawan"));
//        CameraPosition Surabaya = CameraPosition.builder().target(new LatLng(-7.2756368,112.6416433)).zoom(16).bearing(0).tilt(45).build();
//        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(Surabaya));
//
//        if ((ActivityCompat.checkSelfPermission(mView.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) ||
//                (ActivityCompat.checkSelfPermission(mView.getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
//            mGoogleMap.setMyLocationEnabled(true);
//        }
    }

    public void getData() {
        new AmbilAPI().execute();
    }

    public void setData(ArrayList<Banjir> banjirs){
        data = banjirs;
    }

    private class AmbilAPI extends AsyncTask<String, Void, ArrayList<Banjir>>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<Banjir> doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient();
            ArrayList<Banjir> hasil = new ArrayList<>();
            SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
            String link = "http://"+sharedPreferences.getString("ip_address", "192.168.1.10")+DokumentasiApp.getUrlApiGetAllBanjir();
            System.out.println(link);
            Request request = new Request.Builder()
                    .url(link)
                    .build();
            String response = "";
            try {
                Response respon = client.newCall(request).execute();
                response = respon.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                JSONArray array = new JSONArray(response);
                for (int i=0; i<array.length(); i++){
                    hasil.add(new Banjir(String.valueOf(array.getJSONObject(i).getInt("tipe_banjir")),
                            "tanggal",
                            String.valueOf(array.getJSONObject(i).getInt("kecamatan")),
                            String.valueOf(array.getJSONObject(i).getInt("kelurahan")),
                            String.valueOf(array.getJSONObject(i).getString("rayon")),
                            String.valueOf(array.getJSONObject(i).getString("geometry")),
                            String.valueOf(array.getJSONObject(i).getString("geometry")),
                            String.valueOf(array.getJSONObject(i).getString("keterangan")),
                            String.valueOf(array.getJSONObject(i).getInt("korban_jiwa")),
                            "luas",
                            String.valueOf(array.getJSONObject(i).getString("lama")),
                            String.valueOf(array.getJSONObject(i).getInt("jenis_banjir")),
                            String.valueOf(array.getJSONObject(i).getString("foto")),
                            array.getJSONObject(i).getInt("id")
                    ));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return hasil;
        }

        @Override
        protected void onPostExecute(ArrayList<Banjir> aVoid) {
            super.onPostExecute(aVoid);
            setData(aVoid);
            adapter = new CustomAdapter(data);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
//            pb.setVisibility(View.INVISIBLE);
            srl.setRefreshing(false);
        }
    }
}
