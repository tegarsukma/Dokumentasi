package com.crocodev.tegar.dokumentasi;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.crocodev.tegar.dokumentasi.application.DokumentasiApp;
import com.crocodev.tegar.dokumentasi.model.CurahHujan;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class CurahHujanFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    Daftar daftar = new Daftar();
    Spinner rayon, camat, lurah;
    String [] def = {"Pilih Kelurahan"};
    View mView;

    EditText curah;
    Button kirim;
    CurahHujan ch;

    public CurahHujanFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_curah_hujan, container, false);

        rayon      = (Spinner) mView.findViewById         (R.id.rayon);
        camat      = (Spinner) mView.findViewById     (R.id.kecamatan);
        lurah      = (Spinner) mView.findViewById     (R.id.kelurahan);

        curah = (EditText) mView.findViewById(R.id.curah_hujan);
        kirim = (Button) mView.findViewById(R.id.fab);
        initialize();
        return mView;
    }

    void initialize(){
        ArrayAdapter<String> adapter;
        // rayon
        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, daftar.getRayon());
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        rayon.setAdapter(adapter);

        // kecamatan
        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, def);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        camat.setAdapter(adapter);

        // kelurahan
        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, def);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        lurah.setAdapter(adapter);

        rayon.setOnItemSelectedListener(this);
        camat.setOnItemSelectedListener(this);
        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kirim();
            }
        });
    }

    void kirim(){
        ch.setKecamatan(camat.getSelectedItem().toString());
        ch.setKelurahan(lurah.getSelectedItem().toString());
        ch.setCurah(curah.getEditableText().toString());
        new KirimAPI().execute(ch);
    }

    private class KirimAPI extends AsyncTask<CurahHujan, Integer, CurahHujan>{
        ProgressDialog progress;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(getContext());
            progress.setMessage("Mengirim...");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.show();
        }
        @Override
        protected CurahHujan doInBackground(CurahHujan... params) {
            OkHttpClient client = new OkHttpClient();
            Response response;
            Request request;

            RequestBody formBody = new FormBody.Builder()
                    .add("kecamatan", params[0].getKecamatan())
                    .add("kelurahan", params[0].getKelurahan())
                    .add("curah_hujan", params[0].getCurah())
                    .build();

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(DokumentasiApp.getContext());
            String link = "http://"+sharedPreferences.getString("ip_address", "192.168.1.26")+DokumentasiApp.getUrlApiCreateCuHu();
            request = new Request.Builder()
                    .url(link)
                    .post(formBody)
                    .build();
            try {
                response = client.newCall(request).execute();
                System.out.println(response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return params[0];
        }

        @Override
        protected void onPostExecute(CurahHujan curahHujan) {
            super.onPostExecute(curahHujan);
            rayon.setSelection(0);
            curah.setText("");
            progress.dismiss();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        SetSpinnerList ssl = new SetSpinnerList(rayon, camat, lurah, this);
        ssl.onItemSelected(parent, view, position, id);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
