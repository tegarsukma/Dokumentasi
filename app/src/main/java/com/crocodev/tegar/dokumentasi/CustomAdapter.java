package com.crocodev.tegar.dokumentasi;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crocodev.tegar.dokumentasi.model.Banjir;

import java.util.ArrayList;

/**
 * Created by j on 14/04/2017.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";
    private ArrayList<Banjir> data = new ArrayList<>();

    public CustomAdapter(ArrayList<Banjir> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_banjir_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.getKecamatan().setText(String.valueOf(data.get(position).getKecamatan()));
        holder.getKelurahan().setText(String.valueOf(data.get(position).getKelurahan()));
        holder.getTanggal().setText(String.valueOf(data.get(position).getKronologi()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView kecamatan;
        private TextView kelurahan;
        private TextView tanggal;

        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Element " + getAdapterPosition() + " clicked.");
                }
            });
            kecamatan = (TextView) v.findViewById(R.id.tv_kecamatan);
            kelurahan = (TextView) v.findViewById(R.id.tv_kelurahan);
            tanggal = (TextView) v.findViewById(R.id.tv_tanggal);
        }

        public TextView getKelurahan() {
            return kelurahan;
        }

        public TextView getTanggal() {
            return tanggal;
        }

        public TextView getKecamatan() {
            return kecamatan;
        }
    }
}
