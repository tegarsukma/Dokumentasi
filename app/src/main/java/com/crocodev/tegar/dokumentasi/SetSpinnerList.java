package com.crocodev.tegar.dokumentasi;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Created by j on 17/04/2017.
 * Spinner Rayon, Kecamatan, dan Kelurahan dalam satu tempat
 */

class SetSpinnerList {
    private Daftar daftar = new Daftar();
    private Spinner rayon, camat, lurah;
    private String [] def = {"Pilih Kelurahan"};
    private Fragment ctx;

    SetSpinnerList(Spinner rayon, Spinner camat, Spinner lurah, Fragment ctx) {
        this.rayon = rayon;
        this.camat = camat;
        this.lurah = lurah;
        this.ctx = ctx;
    }

    void onItemSelected(AdapterView<?> parent, View view, int position, long id){
        String select = parent.getSelectedItem().toString();
        ArrayAdapter<String> adapter;
        switch (parent.getId()){
            case R.id.rayon:
                Toast.makeText(ctx.getActivity(), "Click on Rayon Spinner", Toast.LENGTH_SHORT).show();
                adapter = new ArrayAdapter<>(ctx.getActivity(),
                        android.R.layout.simple_spinner_item, daftar.getKecamatan1()[position]);
                adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                camat.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;
            case R.id.kecamatan:
                Toast.makeText(ctx.getActivity(), "Click on Kecamatan Spinner", Toast.LENGTH_SHORT).show();
                switch (select){
                    case "Asemrowo":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan1()[0]);
                        break;
                    case "Bubutan":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan1()[1]);
                        break;
                    case "Genteng":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan1()[2]);
                        break;
                    case "Gubeng":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan1()[3]);
                        break;
                    case "Sawahan":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan1()[4]);
                        break;
                    case "Bulak":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan2()[0]);
                        break;
                    case "Kenjeran":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan2()[1]);
                        break;
                    case "Krembangan":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan2()[2]);
                        break;
                    case "Pabean Cantikan":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan2()[3]);
                        break;
                    case "Semampir":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan2()[4]);
                        break;
                    case "Simokerto":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan2()[5]);
                        break;
                    case "Gununganyar":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan3()[0]);
                        break;
                    case "Mulyorejo":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan3()[1]);
                        break;
                    case "Rungkut":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan3()[2]);
                        break;
                    case "Sukolilo":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan3()[3]);
                        break;
                    case "Tambaksari":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan3()[4]);
                        break;
                    case "Tenggilis Mejoyo":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan3()[5]);
                        break;
                    case "Dukuh Pakis":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan4()[0]);
                        break;
                    case "Gayungan":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan4()[1]);
                        break;
                    case "Jambangan":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan4()[2]);
                        break;
                    case "Tegal Sari":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan4()[3]);
                        break;
                    case "Wonocolo":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan4()[4]);
                        break;
                    case "Wonokromo":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan4()[5]);
                        break;
                    case "Benowo":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[0]);
                        break;
                    case "Karang Pilang":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[1]);
                        break;
                    case "Lakar Santri":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[2]);
                        break;
                    case "Pakal":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[3]);
                        break;
                    case "Sambikerep":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[4]);
                        break;
                    case "Sukomanunggal":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[5]);
                        break;
                    case "Tandes":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[6]);
                        break;
                    case "Wiyung":
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item,
                                daftar.getKelurahan5()[7]);
                        break;
                    default:
                        adapter = new ArrayAdapter<>(ctx.getActivity(),
                                android.R.layout.simple_spinner_item, def);
                }
//                adapter = new ArrayAdapter<>(ctx.getActivity(),
//                                android.R.layout.simple_spinner_item, daftar.getKelurahan6().get(position)[0]);
                adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                lurah.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;
        }
    }
}
