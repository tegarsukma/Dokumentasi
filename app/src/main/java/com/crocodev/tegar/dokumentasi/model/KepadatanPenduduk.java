package com.crocodev.tegar.dokumentasi.model;

/**
 * Created by j on 17/04/2017.
 */

public class KepadatanPenduduk {
    private String kecamatan, kelurahan, jumlah, tahun;

    public KepadatanPenduduk() {

    }

    public KepadatanPenduduk(String kecamatan, String kelurahan, String jumlah, String tahun) {
        this.kecamatan = kecamatan;
        this.kelurahan = kelurahan;
        this.jumlah = jumlah;
        this.tahun = tahun;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

}
