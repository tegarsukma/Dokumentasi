package com.crocodev.tegar.dokumentasi.model;

/**
 * Created by j on 04/04/2017.
 */

public class Banjir {
    private String tipe, tanggal, kecamatan, kelurahan, rayon, lat,lng, kronologi, korban, luas, lama,jenis, foto;
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Banjir() {
    }

    public Banjir(String tipe, String tanggal, String kecamatan, String kelurahan, String rayon, String lat,String lng, String kronologi, String korban, String luas, String lama, String jenis, String foto, int id) {
        this.tipe = tipe;
        this.tanggal = tanggal;
        this.kecamatan = kecamatan;
        this.kelurahan = kelurahan;
        this.rayon = rayon;
        this.lat = lat;
        this.lng = lng;
        this.kronologi = kronologi;
        this.korban = korban;
        this.luas = luas;
        this.lama = lama;
        this.jenis = jenis;
        this.foto = foto;
        this.id = id;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getRayon() {
        return rayon;
    }

    public void setRayon(String rayon) {
        this.rayon = rayon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getKronologi() {
        return kronologi;
    }

    public void setKronologi(String kronologi) {
        this.kronologi = kronologi;
    }

    public String getKorban() {
        return korban;
    }

    public void setKorban(String korban) {
        this.korban = korban;
    }

    public String getLuas() {
        return luas;
    }

    public void setLuas(String luas) {
        this.luas = luas;
    }

    public String getLama() {
        return lama;
    }

    public void setLama(String lama) {
        this.lama = lama;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
